package com.example.easymoneysend;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class HistoryActivity extends AppCompatActivity {
    RecyclerView recycler_view;
//    RecyclerAdapter recyclerAdapter;
//    List<History>
    private static final int HISTORY_ACTIVITY_ID = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setTitle(R.string.history_long_text);


        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(HistoryActivity.this, getString(R.string.getting_history));
        String url = GlobalProperties.getBase_url()+"?history";
        RequestQueue queue = Volley.newRequestQueue(HistoryActivity.this);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getBoolean("success")) {
//                                JSONObject data = response.getJSONObject("data");

                                JSONArray data = response.getJSONArray("data");
                                LinkedList<HistoryLine> historyLines = new LinkedList<HistoryLine>();

                                for(int i = 0; i < data.length(); i++) {
                                    System.out.println(data.getJSONObject(i).toString());
                                    historyLines.add(
                                        new HistoryLine(
                                            data.getJSONObject(i).getString("transaction_code"),
                                            data.getJSONObject(i).getString("sender_phone"),
                                            data.getJSONObject(i).getString("amount"),
                                            data.getJSONObject(i).getString("receiver_fullname"),
                                            data.getJSONObject(i).getString("sender_fullname"),
                                            data.getJSONObject(i).getString("receiver_phone"),
                                            data.getJSONObject(i).getString("status")
                                        )
                                    );
                                }

                                HistoryAdapter historyAdapter = new HistoryAdapter(HistoryActivity.this, historyLines);
                                ListView transactions_list_view = (ListView) findViewById(R.id.transactions_list_view);
                                transactions_list_view.setDividerHeight(0);
                                transactions_list_view.setAdapter(historyAdapter);

//                                ArrayAdapter arrayAdapter = new ArrayAdapter<JSONObject>(this, R.layout.history_item_layout, )
//                                CustomAlertDialog alertDialog = new CustomAlertDialog(HistoryActivity.this, getString(R.string.response_ok), response.toString());
//                                alertDialog.show();
//                              recyclerAdapter = new RecyclerAdapter();


                            } else {
                                CustomAlertDialog alertDialog = new CustomAlertDialog(HistoryActivity.this, getString(R.string.response_error), response.getString("error"));
                                alertDialog.show();
                            }


                        } catch (Exception e) {
                            CustomAlertDialog alertDialog = new CustomAlertDialog(HistoryActivity.this, getString(R.string.response_error), e.getMessage());
                            alertDialog.show();
                        }
                        customProgressDialog.dismiss();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomAlertDialog alertDialog = new CustomAlertDialog(HistoryActivity.this, getString(R.string.response_error), error.toString());
                        alertDialog.show();
                        customProgressDialog.dismiss();
                    }
                }
        );

        queue.add(getRequest);


    }
}

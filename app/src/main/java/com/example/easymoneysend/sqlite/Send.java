package com.example.easymoneysend.sqlite;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Send {

    // transaction id
    private int id;

    // sender
    private String senderFirstname;
    private String senderLastname;
    private String senderPhone;
    private String amountToSend;

    // receiver
    private String receiverPhone;
    private String receiverFirstname;
    private String receiverLastname;
    private String transactionCode;
    private String created_at;

    public Send() {
    }

    public Send(String senderFirstname, String senderLastname, String senderPhone,
                String amountToSend, String receiverPhone, String receiverFirstname,
                String receiverLastname, String transactionCode) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date now = new Date();
        String created_at = dateFormat.format(now);

        this.senderFirstname = senderFirstname;
        this.senderLastname = senderLastname;
        this.senderPhone = senderPhone;
        this.amountToSend = amountToSend;
        this.receiverPhone = receiverPhone;
        this.receiverFirstname = receiverFirstname;
        this.receiverLastname = receiverLastname;
        this.transactionCode = transactionCode;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenderFirstname() {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname) {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname() {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname) {
        this.senderLastname = senderLastname;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getAmountToSend() {
        return amountToSend;
    }

    public void setAmountToSend(String amountToSend) {
        this.amountToSend = amountToSend;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverFirstname() {
        return receiverFirstname;
    }

    public void setReceiverFirstname(String receiverFirstname) {
        this.receiverFirstname = receiverFirstname;
    }

    public String getReceiverLastname() {
        return receiverLastname;
    }

    public void setReceiverLastname(String receiverLastname) {
        this.receiverLastname = receiverLastname;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Send{" +
                "id=" + id +
                ", senderFirstname='" + senderFirstname + '\'' +
                ", senderLastname='" + senderLastname + '\'' +
                ", senderPhone='" + senderPhone + '\'' +
                ", amountToSend='" + amountToSend + '\'' +
                ", receiverPhone='" + receiverPhone + '\'' +
                ", receiverFirstname='" + receiverFirstname + '\'' +
                ", receiverLastname='" + receiverLastname + '\'' +
                ", transactionCode='" + transactionCode + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }

//    public int insertSendTransaction(Send transaction) {
////        DB_Transaction db_transaction = new DB_Transaction(getApplicationContext());
////
////        return db_transaction.getDb().insert();
//    }



}

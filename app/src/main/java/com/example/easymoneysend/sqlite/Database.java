package com.example.easymoneysend.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Database extends SQLiteOpenHelper {

    public static final String SEND_TABLE = "send_table";
    public static final String COL_ID = "send_table";
    public static final String COL_SENDER_POHONE = "send_table";
    public static final String COL_SENDER_FIRSTNAME = "send_table";
    public static final String COL_SENDER_LASTNAME = "send_table";
    public static final String COL_AMOUNT_TO_SEND = "send_table";
    public static final String COL_RECEIVER_FIRSTNAME = "send_table";
    public static final String COL_RECEIVER_LASTNAME = "send_table";
    public static final String COL_RECEIVER_PHONE = "send_table";
    public static final String COL_CREATED_AT = "send_table";
    public static final String COL_TRANSACTION_CODE = "send_table";

    private static final String sql_send_table = "CREATE TABLE " + SEND_TABLE + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_SENDER_FIRSTNAME + " TEXT NOT NULL, " +
            COL_SENDER_LASTNAME + " TEXT NOT NULL, " +
            COL_SENDER_POHONE + " TEXT NOT NULL, " +
            COL_AMOUNT_TO_SEND+ " REAL NOT NULL, " +
            COL_RECEIVER_FIRSTNAME + " TEXT NOT NULL, " +
            COL_RECEIVER_LASTNAME + " TEXT NOT NULL, " +
            COL_RECEIVER_PHONE + " TEXT NOT NULL, " +
            COL_TRANSACTION_CODE + " TEXT NOT NULL, " +
            COL_CREATED_AT + " TEXT NOT NULL " +
            ");"
            ;

    public Database(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.sql_send_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE "+ SEND_TABLE + ";");
        db.execSQL(this.sql_send_table);
    }
}

package com.example.easymoneysend;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.chip.Chip;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class HistoryAdapter extends ArrayAdapter<HistoryLine> {
    private  final  Context context;
    private  LinkedList<HistoryLine> historyLines;
    int _position;
    public HistoryAdapter(Context context, LinkedList<HistoryLine> historyLines) {
        super(context, 0, historyLines);
        this.context = context;
        this.historyLines = historyLines;
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.history_item_layout, parent, false);
        }
//        else {
//            convertView = (LinearLayout) convertView;
//        }

        TextView transaction_code = (TextView) convertView.findViewById(R.id.item_transaction_code);
        TextView amount = (TextView) convertView.findViewById(R.id.amount);
        Chip status = (Chip) convertView.findViewById(R.id.transaction_status);
//        Chip del_transaction_btn = (Chip) convertView.findViewById(R.id.transaction_status);

        _position = position;



        transaction_code.setText(historyLines.get(position).getTransaction_code());
        amount.setText(historyLines.get(position).getAmount() + context.getString(R.string.devise));
        status.setText(historyLines.get(position).getStatus());

        if(status.getText().equals("pending")) {
            status.setChipBackgroundColorResource(R.color.green);
        } else {
            status.setChipBackgroundColorResource(R.color.red);
        }

        return convertView;
    }
}

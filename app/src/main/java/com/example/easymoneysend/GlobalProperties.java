package com.example.easymoneysend;

public class GlobalProperties {
    private static String base_url = "http://easytransac.atspace.cc/";
    //private static String base_url = "http://192.168.42.142/apieasytransac/";
    private static boolean isLogedIn = false;
    public GlobalProperties() {
    }

    public static boolean getLoginState() {
        return GlobalProperties.isLogedIn;
    }

    public static void setLoginState(boolean value) {
        GlobalProperties.isLogedIn = value;
    }

    public static double getFeeChargePrice(double amount_to_send) {
        double fees = 0.0;
        if(amount_to_send <= 10000 ) {
            fees = (amount_to_send * 3) / 100;
        }

        else if(amount_to_send <= 100000) {
            fees = (amount_to_send * 4) / 100;
        }

        else if(amount_to_send <= 200000) {
            fees = (amount_to_send * 6) / 100;
        }

        else if(amount_to_send < 400000) {
            fees = (amount_to_send * 8.5) / 100;
        }

        else if(amount_to_send <= 800000) {
            fees = (amount_to_send * 9.5) / 100;
        }

        else if(amount_to_send <= 2000000) {
            fees = (amount_to_send * 11) / 100;
        }

        else if(amount_to_send <= 5000000) {
            fees = (amount_to_send * 10) / 100;
        }

        else {
            fees = (amount_to_send * 9) / 100;
        }

        return fees;
    }

    public static String getBase_url() {
        return base_url;
    }
}

package com.example.easymoneysend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ConfirmWithdrawalActivity extends AppCompatActivity {

    private static final int CONFIRM_WITHDRAWAL_ACTIVITY_ID = 3;

    private String receiverFirstname;
    private String receiverLastname;
    private String identityType;
    private String identityNumber;
    private String transactionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_withdrawal);
        setTitle(R.string.withdrawal_long_text);

        Intent intent = getIntent();

        receiverFirstname = intent.getStringExtra("receiverFirstname");
        receiverLastname = intent.getStringExtra("receiverLastname");
        identityType = intent.getStringExtra("identityType");
        identityNumber = intent.getStringExtra("identityNumber");
        transactionCode = intent.getStringExtra("transactionCode");

        ((TextView) findViewById(R.id.receiver_receiver_firstname)).setText(receiverFirstname);
        ((TextView) findViewById(R.id.receiver_receiver_lastname)).setText(receiverLastname);
        ((TextView) findViewById(R.id.receiver_receiver_identity_type)).setText(identityType);
        ((TextView) findViewById(R.id.receiver_receiver_identity_number)).setText(identityNumber);
        ((TextView) findViewById(R.id.receiver_transaction_code)).setText(transactionCode);

        Button confirmeWithdrawalButton = (Button) findViewById(R.id.confirmeWithdrawalButton);

        confirmeWithdrawalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = getString(R.string.confirm_withdrawal_alert_dialog_message)+"\n\n"+
                        getString(R.string.firstname)+": "+receiverFirstname+"\n"+
                        getString(R.string.lastname)+": "+receiverLastname+"\n"+
                        getString(R.string.identity_type)+": "+identityType+"\n"+
                        getString(R.string.identity_number)+": "+identityNumber+"\n"+
                        getString(R.string.transaction_code)+": "+transactionCode +"\n";

                String title = getString(R.string.confirm_withdrawal_alert_dialog_title);

                CustomAlertDialog alert = new CustomAlertDialog(v.getContext(), title, message);

                alert.setPositiveButton(getString(R.string.validate), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(ConfirmWithdrawalActivity.this, getString(R.string.processing));

                        String url = GlobalProperties.getBase_url()+"?withdrawal_transaction" +
                            "&identity_type=" + identityType +
                            "&identity_number=" + identityNumber +
                            "&receiver_firstname=" + receiverFirstname +
                            "&receiver_lastname=" + receiverLastname +
                            "&transaction_code=" + transactionCode
                        ;

                        RequestQueue queue = Volley.newRequestQueue(ConfirmWithdrawalActivity.this);
                        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                                new Response.Listener<JSONObject>()
                                {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if(response.getBoolean("success")) {
                                                JSONObject data = response.getJSONObject("data");
                                                Double amount = data.getDouble("amount");
                                                CustomAlertDialog alertDialog = new CustomAlertDialog(
                                                        ConfirmWithdrawalActivity.this,
                                                        getString(R.string.response_ok),
                                                        data.getString("message")+"\n"+"Withdrawal Amount: "+ amount + " " +getString(R.string.devise));

                                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        toMainMenuActivity();
                                                    }
                                                });

                                                alertDialog.show();

                                            } else {
                                                CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmWithdrawalActivity.this, getString(R.string.response_error), response.getString("error"));
                                                alertDialog.show();
                                            }


                                        } catch (Exception e) {
                                            CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmWithdrawalActivity.this, getString(R.string.error), e.getMessage());
                                            alertDialog.show();
                                        }
                                        customProgressDialog.dismiss();
                                    }
                                },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmWithdrawalActivity.this, getString(R.string.response_error), error.toString());
                                        alertDialog.show();
                                        customProgressDialog.dismiss();
                                    }
                                }
                        );

                        queue.add(getRequest);
                    }
                });

                alert.show();
            }
        });
    }

    private void toMainMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }
}

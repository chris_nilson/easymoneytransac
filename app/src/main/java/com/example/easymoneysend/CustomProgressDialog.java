package com.example.easymoneysend;

import android.app.ProgressDialog;
import android.content.Context;

public class CustomProgressDialog extends ProgressDialog {


    public CustomProgressDialog(Context context) {
        super(context);
        setMessage(context.getString(R.string.processing));
        setCancelable(false);
        show();
    }

    public CustomProgressDialog(Context context, boolean cancelable) {
        super(context);
        setMessage("Processing");
        setCancelable(cancelable);
        show();
    }

    public CustomProgressDialog(Context context, String message) {
        super(context);
        setMessage(message);
        show();
    }

    public CustomProgressDialog(Context context, String message,boolean cancelable) {
        super(context);
        setMessage(message);
        setCancelable(cancelable);
        show();
    }
}

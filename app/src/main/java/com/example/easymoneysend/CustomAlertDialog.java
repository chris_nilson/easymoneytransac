package com.example.easymoneysend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public class CustomAlertDialog extends AlertDialog.Builder {

    public CustomAlertDialog(Context context, String title, String message) {
        super(context);
        this.setTitle(title);
        this.setMessage(message);
    }

    public CustomAlertDialog(Context context, String title, String message, String positive) {
        super(context);
        this.setTitle(title);
        this.setMessage(message);
        this.setPositiveButton(positive, null);
    }

}

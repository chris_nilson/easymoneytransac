package com.example.easymoneysend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.view.View;
import android.webkit.JsPromptResult;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import static com.example.easymoneysend.R.*;

public class MainActivity extends AppCompatActivity {

    private static final int MAIN_ACTIVITY_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        Button loginButton = (Button) findViewById(id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login = ((EditText) findViewById(id.loginUsernameField)).getText().toString();
                String password = ((EditText) findViewById(id.loginPasswordField)).getText().toString();

                final CustomProgressDialog customProgressDialog = new CustomProgressDialog(v.getContext(), getString(string.processing_login));
                String url = GlobalProperties.getBase_url()+"?login="+ login +"&password=" + password;
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if(response.getBoolean("success")) {
                                        JSONObject data = response.getJSONObject("data");
                                        GlobalProperties.setLoginState(true);
                                        toMainMenuActivity();

                                    } else {
                                        CustomAlertDialog alertDialog = new CustomAlertDialog(MainActivity.this, getString(R.string.response_error), response.getString("error"));
                                        alertDialog.show();
                                    }


                                } catch (Exception e) {
                                    CustomAlertDialog alertDialog = new CustomAlertDialog(MainActivity.this, getString(R.string.response_error), e.getMessage());
                                    alertDialog.show();
                                }
                                customProgressDialog.dismiss();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                CustomAlertDialog alertDialog = new CustomAlertDialog(MainActivity.this, getString(R.string.response_error), error.toString());
                                alertDialog.show();
                                customProgressDialog.dismiss();
                            }
                        }
                );

                queue.add(getRequest);



            }

        });
    }

    private void toMainMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivityForResult(intent, MAIN_ACTIVITY_ID);
        dismissKeyboardShortcutsHelper();
        finish();
    }

    @Override
    protected void onActivityResult(int req, int res, Intent data) {
        if(res == MAIN_ACTIVITY_ID) {
            if(GlobalProperties.getLoginState()) { // toujours connecte
                //toMainMenuActivity();
                finish();
            } else { // deconnecte, alors on quitte
                finish();
            }
        }
    }

}

package com.example.easymoneysend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;


public class ConfirmSendActivity extends AppCompatActivity {

    private static final int CONFIRM_SEND_ACTIVITY_ID = 2;

    private String senderFirstname;
    private String senderLastname;
    private String senderPhone;
    private String amountToSend;
    private String receiverFirstname;
    private String receiverLastname;
    private String receiverPhone;
    private double fee_charge_price;
    int transaction_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_send);
        setTitle(R.string.deposit_long_text);

        Intent intent = getIntent();

        senderFirstname = intent.getStringExtra("senderFirstname");
        senderLastname = intent.getStringExtra("senderLastname");
        senderPhone = intent.getStringExtra("senderPhone");
        amountToSend = intent.getStringExtra("amountToSend");
        receiverFirstname = intent.getStringExtra("receiverFirstname");
        receiverLastname = intent.getStringExtra("receiverLastname");
        receiverPhone = intent.getStringExtra("receiverPhone");

        fee_charge_price = GlobalProperties.getFeeChargePrice(Double.parseDouble(amountToSend));

        System.out.println("Fee Charges: "+fee_charge_price);
//
        ( (TextView) findViewById(R.id.sender_sender_lastname)).setText(senderFirstname);
        ( (TextView) findViewById(R.id.sender_sender_firstname)).setText(senderLastname);
        ( (TextView) findViewById(R.id.sender_sender_phone)).setText(senderPhone);
        ( (TextView) findViewById(R.id.sender_amount_to_send)).setText(amountToSend+" "+ getString(R.string.devise));
        ( (TextView) findViewById(R.id.sender_receiver_firstname)).setText(receiverFirstname);
        ( (TextView) findViewById(R.id.sender_receiver_lastname)).setText(receiverLastname);
        ( (TextView) findViewById(R.id.sender_receiver_phone)).setText(receiverPhone);
        ( (TextView) findViewById(R.id.fee_charge_price_value)).setText(Double.toString(fee_charge_price)+" "+getString(R.string.devise));
//
//
        Button confirmeDepositButton = (Button) findViewById(R.id.confirmeDepositButton);

        confirmeDepositButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = getString(R.string.confirm_deposit_alert_dialog_title);

                String message = getString(R.string.confirm_deposit_alert_dialog_message)+"\n\n"+
                        getString(R.string.firstname)+": "+senderFirstname+"\n"+
                        getString(R.string.lastname)+": "+senderLastname+"\n"+
                        getString(R.string.sender_phone)+": "+senderPhone+"\n"+
                        getString(R.string.receiver_phone)+": "+receiverPhone+"\n"+
                        getString(R.string.amount)+": "+amountToSend+" "+ getString(R.string.devise) +"\n";

                final CustomAlertDialog alert = new CustomAlertDialog(v.getContext(), title, message);

                alert.setPositiveButton(getString(R.string.validate), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(
                                ConfirmSendActivity.this, getString(R.string.processing));

                        String url = GlobalProperties.getBase_url()+"?deposit_transaction" +
                            "&sender_firstname=" + senderFirstname +
                            "&sender_lastname=" + senderLastname +
                            "&sender_phone=" + senderPhone +
                            "&amount_to_send=" + amountToSend +
                            "&receiver_firstname=" + receiverFirstname +
                            "&receiver_lastname=" + receiverLastname +
                            "&receiver_phone=" + receiverPhone
                        ;

                        RequestQueue queue = Volley.newRequestQueue(ConfirmSendActivity.this);
                        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, null,
                                new Response.Listener<JSONObject>()
                                {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if(response.getBoolean("success")) {
                                                JSONObject data = response.getJSONObject("data");
                                                transaction_code = data.getInt("transaction_code");
                                                CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmSendActivity.this, getString(R.string.response_ok), data.getString("message"));

                                                alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        toMainMenuActivity();
                                                        toTransactionDoneActivity(transaction_code);
                                                    }
                                                });

                                                alertDialog.show();

                                            } else {
                                                CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmSendActivity.this, getString(R.string.response_error), response.getString("error"));
                                                alertDialog.show();
                                            }


                                        } catch (Exception e) {
                                            CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmSendActivity.this, getString(R.string.response_error), e.getMessage());
                                            alertDialog.show();
                                        }
                                        customProgressDialog.dismiss();
                                    }
                                },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        CustomAlertDialog alertDialog = new CustomAlertDialog(ConfirmSendActivity.this, getString(R.string.response_error), error.toString());
                                        alertDialog.show();
                                        customProgressDialog.dismiss();
                                    }
                                }
                        );

                        queue.add(getRequest);
                    }
                });

                alert.show();
            }
        });
    }

    private void toMainMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    private void toTransactionDoneActivity(int transaction_code) {
        Log.d("transaction_code", Integer.toString(transaction_code));
        Intent intent = new Intent(this, TransactionDoneActivity.class);
        intent.putExtra("transaction_code", transaction_code);
        startActivity(intent);
        finish();
    }
}

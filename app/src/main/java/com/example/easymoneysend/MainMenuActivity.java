package com.example.easymoneysend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.chip.Chip;

public class MainMenuActivity extends AppCompatActivity {

    private static final int MENU_ACTIVITY_ID = 1;

    private String senderFirstname;
    private String senderLastname;
    private String senderPhone;
    private String receiverPhone;
    private String amountToSend;
    private String sender_receiver_Firstname;
    private String sender_receiver_Lastname;

    private String receiverFirstname;
    private String receiverLastname;
    private String identityType;
    private String identityNumber;
    private String transactionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        setTitle(R.string.dashboard);

        Button depositButton = (Button) findViewById(R.id.depositButton);
        Button withdrawalButton = (Button) findViewById(R.id.withdrawalButton);
        Button validateDeposit = (Button) findViewById(R.id.validateDeposit);
        Button validateWithdrawal = (Button) findViewById(R.id.validateWithdrawal);
        Button historyButton = (Button) findViewById(R.id.historyButton);
        Chip deconnectionButton = (Chip) findViewById(R.id.disconnection_button);

        deconnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalProperties.setLoginState(false);
                toMainActivity();
                finish();
            }
        });


        depositButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.withdrawalFormScrollView).setVisibility(View.GONE);
                findViewById(R.id.depositFormScrollView).setVisibility(View.VISIBLE);
                setTitle(R.string.deposit_long_text);
            }
        });

        withdrawalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.depositFormScrollView).setVisibility(View.GONE);
                findViewById(R.id.withdrawalFormScrollView).setVisibility(View.VISIBLE);
                setTitle(R.string.withdrawal_long_text);
            }
        });

        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toHistoryActivity();
            }
        });

        validateWithdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                receiverFirstname = ((EditText) findViewById(R.id.receiverFirstName)).getText().toString();
                receiverLastname = ((EditText) findViewById(R.id.receiverLastname)).getText().toString();
                identityType = ((EditText) findViewById(R.id.identityType)).getText().toString();
                identityNumber = ((EditText) findViewById(R.id.identityNumber)).getText().toString();
                transactionCode = ((EditText) findViewById(R.id.transactionCode)).getText().toString();

                if(receiverLastname.isEmpty() || receiverFirstname.isEmpty() || identityType.isEmpty() || identityNumber.isEmpty() || transactionCode.isEmpty()) {
                    CustomAlertDialog alertDialog = new CustomAlertDialog(v.getContext(), getString(R.string.error), getString(R.string.error_fields_empty), getString(R.string.close));
                    alertDialog.show();
                } else {
                    toConfirmWithdrawalActivity();
                }

            }
        });

        validateDeposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                senderFirstname = ((EditText) findViewById(R.id.senderFirstname)).getText().toString();
                senderLastname = ((EditText) findViewById(R.id.senderLastname)).getText().toString();
                senderPhone = ((EditText) findViewById(R.id.senderPhone)).getText().toString();
                receiverPhone = ((EditText) findViewById(R.id.receiverPhone)).getText().toString();
                amountToSend = ((EditText) findViewById(R.id.amountToSend)).getText().toString();
                sender_receiver_Firstname = ((EditText) findViewById(R.id.sender_receiverFirstName)).getText().toString();
                sender_receiver_Lastname = ((EditText) findViewById(R.id.sender_receiverLastName)).getText().toString();

                if(senderFirstname.isEmpty() || senderLastname.isEmpty() || senderPhone.isEmpty() || receiverPhone.isEmpty() || amountToSend.isEmpty() || sender_receiver_Firstname.isEmpty() || sender_receiver_Lastname.isEmpty()) {
                    CustomAlertDialog alertDialog = new CustomAlertDialog(v.getContext(), getString(R.string.error), getString(R.string.error_fields_empty), getString(R.string.close));
                    alertDialog.show();
                } else {
                    toConfirmSendActivity();
                }

            }
        });
    }

    private void toConfirmSendActivity() {
        Intent intent = new Intent(this, ConfirmSendActivity.class);
        intent.putExtra("senderFirstname", senderFirstname);
        intent.putExtra("senderLastname", senderLastname);
        intent.putExtra("senderPhone", senderPhone);
        intent.putExtra("amountToSend", amountToSend);
        intent.putExtra("receiverFirstname", sender_receiver_Firstname);
        intent.putExtra("receiverLastname", sender_receiver_Lastname);
        intent.putExtra("receiverPhone", receiverPhone);


        startActivity(intent);
    }

    private void toConfirmWithdrawalActivity() {
        Intent intent = new Intent(this, ConfirmWithdrawalActivity.class);
        intent.putExtra("receiverFirstname", receiverFirstname);
        intent.putExtra("receiverLastname", receiverLastname);
        intent.putExtra("identityType", identityType);
        intent.putExtra("identityNumber", identityNumber);
        intent.putExtra("transactionCode", transactionCode);

        startActivity(intent);
    }

    private void toMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent, MENU_ACTIVITY_ID);
    }

    private void toHistoryActivity() {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivityForResult(intent, MENU_ACTIVITY_ID);
    }

    @Override
    protected void onActivityResult(int req, int res, Intent intent) {
        if(!GlobalProperties.getLoginState()) {
            toMainActivity();
        }
    }
}

package com.example.easymoneysend;

public class HistoryLine implements java.io.Serializable {
    private static final long serialVersionUID = -1861829790651893668L;
    private String transaction_code;
    private String sender_phone;
    private String amount;
    private String receiver_fullname;
    private String sender_fullname;
    private String receiver_phone;
    private String status;

    public HistoryLine(String transaction_code, String sender_phone, String amount, String receiver_fullname, String sender_fullname, String receiver_phone, String status) {
        this.transaction_code = transaction_code;
        this.sender_phone = sender_phone;
        this.amount = amount;
        this.receiver_fullname = receiver_fullname;
        this.sender_fullname = sender_fullname;
        this.receiver_phone = receiver_phone;
        this.status = status;
    }

    public String getTransaction_code() {
        return this.transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public String getSender_phone() {
        return this.sender_phone;
    }

    public void setSender_phone(String sender_phone) {
        this.sender_phone = sender_phone;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReceiver_fullname() {
        return this.receiver_fullname;
    }

    public void setReceiver_fullname(String receiver_fullname) {
        this.receiver_fullname = receiver_fullname;
    }

    public String getSender_fullname() {
        return this.sender_fullname;
    }

    public void setSender_fullname(String sender_fullname) {
        this.sender_fullname = sender_fullname;
    }

    public String getReceiver_phone() {
        return this.receiver_phone;
    }

    public void setReceiver_phone(String receiver_phone) {
        this.receiver_phone = receiver_phone;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

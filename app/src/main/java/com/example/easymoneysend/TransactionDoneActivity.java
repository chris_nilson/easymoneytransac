package com.example.easymoneysend;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import net.glxn.qrgen.android.QRCode;


public class TransactionDoneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_done);

        Intent intent = getIntent();
        int transaction_code = intent.getIntExtra("transaction_code", 000);

        Bitmap qr_code_image = QRCode.from(Integer.toString(transaction_code)).bitmap();

        ImageView qr_image = findViewById(R.id.qr_code);
        qr_image.setImageBitmap(qr_code_image);


        Button go_home = findViewById(R.id.go_home);
        go_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toMainMenuActivity();
            }
        });
    }

    private void toMainMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }
}
